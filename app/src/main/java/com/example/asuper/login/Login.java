package com.example.asuper.login;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void ingresar(View view){

        // SE BUSCAN LAS CAJAS DE TEXTOS //
        String usuario=((EditText)findViewById(R.id.usuario)).getText().toString();
        String contrasena=((EditText)findViewById(R.id.contrasena)).getText().toString();

        // SE CONSTRUYE EL DIÁLOGO DE TEXTO VACÍO //
        AlertDialog.Builder vacio = new  AlertDialog.Builder(this);
        vacio.setMessage("Por favor ingrese sus datos")
                .setTitle("ALERTA!").setCancelable(false)
                .setNeutralButton("Aceptar",
                        new  DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                dialog.cancel();
                            }
                        });

        // SE CONSTRUYE EL DIÁLOGO DE ALERTA DE BIENVENIDA //
        AlertDialog.Builder bienvenido = new AlertDialog.Builder(this);
        bienvenido.setMessage("Bienvenido!")
                .setTitle("MENSAJE").setCancelable(false)
                .setNeutralButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // SE CONTRUYE EL DIÁLOGO DE ALERTA DE DATOS INCORRECTOS //
        AlertDialog.Builder incorrecto = new  AlertDialog.Builder(this);
        incorrecto.setMessage("Usuario o contraseña incorrectos")
                .setTitle("ALERTA!").setCancelable(false)
                .setNeutralButton("Aceptar",
                        new  DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                dialog.cancel();
                            }
                        });

        // CHECAS SI LOS CAMPOS DE TEXTO ESTÁN VACÍOS //
        if (usuario.equals("") && contrasena.equals("")){
            AlertDialog alert = vacio.create();
            alert.show();

            //CHECAS QUE USUARIO Y CONTRASEÑA SEAN CORRECTOS //
        }else if (usuario.equals("user") && contrasena.equals("pass")) {
            AlertDialog alert = bienvenido.create();
            alert.show();

            // SI NO SON CORRECTOS, ENVÍAS EL DIÁLOGO DE DATOS INCORRECTOS //
        }else{
            AlertDialog alert = incorrecto.create();
            alert.show();
        }
    }

    // Y POS ESTE BORRA LOS CAMPOS DE TEXTO //
    public void cancelar(View view){
        EditText Usuario=(EditText)findViewById(R.id.usuario);
        EditText Contrasena=(EditText)findViewById(R.id.contrasena);

        Usuario.setText("");
        Contrasena.setText("");
    }

}
